# CurrencyConversion - TDD

## Classes

- ### ConvertRequest (Client-Side)
    Acts as a request which contains the information collected at the client side for the conversion transaction. It returns a response to the client.
- ### CurrencyConversion (Server-Side)
  Acts as the server that will receive and process conversion requests. This party owns the currency code database.
