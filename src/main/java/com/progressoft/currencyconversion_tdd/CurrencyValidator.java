package com.progressoft.currencyconversion_tdd;

/*
Acts as an engine for the currencyConverter (car) to work the validation
We created this because if we wanted to introduce a new validator without having
to go change our base code (remember, we want to package our code and stop it from being
edited)
We can create multiple implementations in the test class
 */
@FunctionalInterface
public interface CurrencyValidator {
    public boolean isValid(String code);
}
