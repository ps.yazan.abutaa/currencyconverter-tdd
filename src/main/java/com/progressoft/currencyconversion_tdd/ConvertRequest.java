package com.progressoft.currencyconversion_tdd;

import java.math.BigDecimal;
import java.util.Currency;


public final class ConvertRequest {

    //Making this class final to ensure that from and to do not get changed
    //In the process when for example a developer comes and inherits from this class,
    //the final keyword stops them from overriding the get methods.
    //Final classes cannot be inherited from.
    private final String from;
    private final String to;
    private final BigDecimal amount;

    public ConvertRequest(String from, String to, BigDecimal amount) throws NullPointerException {
        failIfNullCodes(from, to);
        failIfInvalidAmount(amount);

        this.from = from;
        this.to = to;
        this.amount = amount;
    }



    private void failIfInvalidAmount(BigDecimal amount) {
        if (amount == null)
            throw new NullPointerException("Amount is null");

        if (amountIsNegative(amount))
            throw new IllegalArgumentException("Amount is negative");
    }

    private void failIfNullCodes(String from, String to) {
        if (from == null)
            throw new NullPointerException("From is null");

        if (to == null)
            throw new NullPointerException("To is null");
    }

    private boolean amountIsNegative(BigDecimal amount) {
        return amount.signum() < 0;
    }



    public String getFrom() {
        return this.from;
    }

    public String getTo() {
        return this.to;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }
}

