package com.progressoft.currencyconversion_tdd;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DummyRateSupplier implements RateSupplier
{
    @Override
    public BigDecimal getRate(String from, String to) {
        if(from.equals("USD") && to.equals("JOD"))
            return new BigDecimal("0.71");

        if(from.equals("USD") && to.equals("YEN"))
            return new BigDecimal("107.06");

        throw new IllegalArgumentException("No such code");
    }

}

