package com.progressoft.currencyconversion_tdd;

import java.math.BigDecimal;

@FunctionalInterface
public interface RateSupplier {

    public BigDecimal getRate(String from, String to);
}
