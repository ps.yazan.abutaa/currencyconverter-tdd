package com.progressoft.currencyconversion_tdd;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Currency;

public class CurrencyConverter {

    //Applied OOP principle: favor composition over inheritance
    //Applied dependency injection: we use this because it helps us achieve our ultimate goal, which is to
    //Let the code drive us.

    //How does the code drive us in this situation? When you create a new CurrencyConverter, the constructor
    //will ask for a validator. If you had a setter for the validator instead of passing it in the constructor,
    //then how will you know that it requires a validator?

    //SOLID Principles:
    //S: Single responsibility: class and methods responsible for something specific
    //O: Open for extension, closed for modification: things like composition over inheritance. Add new behaviour without
    //changing the existing code.
    //L: Liskov principle: don't call us, we'll call you
    //I: Interface segregation: make sure the components are decoupled through abstraction and interfaces (more details later)
    //D: Dependency injection: as we know it


    final private CurrencyValidator currencyValidator;

    //The following line is called "coupling", and is not considered dependency injection
    //private CurrencyValidator currencyValidator = new someValidator();


    final private RateSupplier rateSupplier;

    public CurrencyConverter(CurrencyValidator currencyValidator, RateSupplier rateSupplier) {
        if(currencyValidator == null)
            throw new NullPointerException("Validator cannot be null");

        if(rateSupplier == null)
            throw new NullPointerException("Supplier cannot be null");

        this.currencyValidator = currencyValidator;
        this.rateSupplier = rateSupplier;
    }

    public BigDecimal convert(ConvertRequest convertRequest)
    {
        failIfInvalidCodes(convertRequest.getFrom(), convertRequest.getTo());

        BigDecimal ratio = rateSupplier.getRate(convertRequest.getFrom(), convertRequest.getTo());

        return convertRequest.getAmount().multiply(ratio);
    }

    private void failIfInvalidCodes(String from, String to)
    {
        if(!isValid(from))
            throw new IllegalArgumentException("Invalid from currency code");

        if(!isValid(to))
            throw new IllegalArgumentException("Invalid to currency code");

    }

    public boolean isValid(String currency) {
        return currencyValidator.isValid(currency);
    }

}
