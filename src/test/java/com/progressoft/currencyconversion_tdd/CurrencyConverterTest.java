package com.progressoft.currencyconversion_tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import java.util.Random;


public class CurrencyConverterTest {


    private static final String INVALID_CODE = "invalid_code";
    private static final String VALID_CODE = "USD";

    private MockValidator mockCurrencyValidator = new MockValidator();
    //MockValidator: a fake validator made to test the implementation of the validator interface

    private class MockValidator implements CurrencyValidator
    {
        @Override
        public boolean isValid(String code) {
            if(code == VALID_CODE)
            return true;

            return false;
        }
    }

    @Test
    public void givenNullValidator_whenConstructor_thenFail()
    {
        //Create an anonymous CurrencyValidator that returns false (refer to notion notes for lambda clean code)
        //CurrencyConverter currencyConverter = new CurrencyConverter((code)->false);
        Exception e = Assertions.assertThrows(NullPointerException.class,
                () ->
                {
                    CurrencyConverter converter = new CurrencyConverter(null,(f,t)->BigDecimal.ZERO);
                });
        Assertions.assertEquals(e.getMessage(),"Validator cannot be null");

    }

    @Test
    public void givenNullRateSupplier_whenConstructor_thenFail()
    {
        //Create an anonymous CurrencyValidator that returns false (refer to notion notes for lambda clean code)
        //CurrencyConverter currencyConverter = new CurrencyConverter((code)->false);

        Exception e = Assertions.assertThrows(NullPointerException.class,
                () ->
                {
                    CurrencyConverter converter = new CurrencyConverter((c)->true,null);
                });
        Assertions.assertEquals(e.getMessage(),"Supplier cannot be null");

    }
    @Test
    public void givenRequestWithInvalidCodes_whenConvert_thenFail()
    {
        CurrencyConverter currencyConverter = new CurrencyConverter((code)->!(code == INVALID_CODE),(f,t)->BigDecimal.ZERO);

        final ConvertRequest invalidFromRequest = new ConvertRequest(INVALID_CODE, "JOD",BigDecimal.valueOf(1));
        Exception exception  = Assertions.assertThrows(IllegalArgumentException.class,
                ()-> currencyConverter.convert(invalidFromRequest));
        Assertions.assertEquals(exception.getMessage(),"Invalid from currency code");

        final ConvertRequest invalidToRequest = new ConvertRequest("JOD", INVALID_CODE,BigDecimal.valueOf(1));
        exception  = Assertions.assertThrows(IllegalArgumentException.class,
                ()-> currencyConverter.convert(invalidToRequest));
        Assertions.assertEquals(exception.getMessage(), "Invalid to currency code");
    }
    @Test
    public void givenValidRequestWithValidCodes_whenConvert_thenSucceed()
    {
        String from  = "USD";
        String to = "JOD";
        BigDecimal amountToConvert = BigDecimal.valueOf(230.5);

        BigDecimal possibleRatios[] = {
                new BigDecimal("0.71"),new BigDecimal("1.41"),new BigDecimal("0.80")
        };
        BigDecimal ratio = possibleRatios[new Random().nextInt(possibleRatios.length)];

        //Creating the request
        ConvertRequest request = new ConvertRequest(from,to,amountToConvert);

        //Create the converter with anonymous implementations for validator and ratio supplier
        CurrencyConverter converter = new CurrencyConverter((c -> true),(f,t)->ratio);

        //Convert the request with our converter
        BigDecimal convertedAmount = converter.convert(request);

        //Make sure the conversion is not null
        Assertions.assertNotNull(convertedAmount, "returned conversion is null");

        //Calculate the expected value
        BigDecimal expected = ratio.multiply(amountToConvert);

        //Compare the expected conversion with the actual conversion
        Assertions.assertEquals(0,convertedAmount.compareTo(expected), "Conversion failed");

    }

}
