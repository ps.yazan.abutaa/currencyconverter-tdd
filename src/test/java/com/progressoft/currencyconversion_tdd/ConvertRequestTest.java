package com.progressoft.currencyconversion_tdd;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.Currency;

public class ConvertRequestTest {

    @Test
    public void givenNullOrInvalidParameters_whenConstruct_thenConstructedAsExpected()
    {
        String from = null;
        String to = null;
        BigDecimal amount = null;

        Exception e  = Assertions.assertThrows(NullPointerException.class,
                ()->new ConvertRequest(null,"",BigDecimal.valueOf(1)),"From is null");
        Assertions.assertEquals(e.getMessage(), "From is null");

        e  = Assertions.assertThrows(NullPointerException.class,
                ()->new ConvertRequest("",null,BigDecimal.valueOf(1)),"To is null");
        Assertions.assertEquals(e.getMessage(), "To is null");

        e  = Assertions.assertThrows(NullPointerException.class,
                ()->new ConvertRequest("","",null),"Amount is null");
        Assertions.assertEquals(e.getMessage(), "Amount is null");

        e  = Assertions.assertThrows(IllegalArgumentException.class,
                ()->new ConvertRequest("USD","JOD",BigDecimal.valueOf(-9.54)),"Amount is negative");
        Assertions.assertEquals(e.getMessage() ,"Amount is negative");

    }

    @Test
    public void givenValidParameters_whenConstruct_thenSuccess()
    {
        String from = "USD";
        String to = "JOD";
        BigDecimal amount = BigDecimal.valueOf(100);

        ConvertRequest cr = new ConvertRequest(from,to,amount);

        Assertions.assertEquals(from,cr.getFrom());
        Assertions.assertEquals(to,cr.getTo());
        Assertions.assertEquals(amount, cr.getAmount());

    }
}
